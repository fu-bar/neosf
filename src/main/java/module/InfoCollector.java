package module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import helpers.ReflectionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections8.Reflections;
import org.reflections8.scanners.ResourcesScanner;
import osf.shared.exceptions.OsfException;
import structures.LegacyModuleInfo;
import structures.ModuleInfo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class InfoCollector {
    private final Logger log = LogManager.getLogger(InfoCollector.class);
    private final Reflections reflections = new Reflections(new ResourcesScanner());
    private final Set<String> nonModuleInfoFileNames = Set.of("osf.json");

    public InfoCollector() {
        // Using the given class-path
    }

    public InfoCollector(String rootJarsFolder, String jarNamesRegex) {
        this(List.of(rootJarsFolder), jarNamesRegex);
    }

    public InfoCollector(Collection<String> rootJarsFolders, String jarNamesRegex) {
        ReflectionHelper reflectionHelper = new ReflectionHelper();
        reflectionHelper.discoverJars(rootJarsFolders, jarNamesRegex);
    }

    public Set<ModuleInfo> collectInfoOnModules() {
        log.debug("Collecting information on osf modules");
        ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
        Set<ModuleInfo> moduleInfos = new HashSet<>();
        Set<String> yamlResourceNames = reflections.getResources(Pattern.compile(".*\\.yaml"));
        Set<String> jsonResourceNames = reflections.getResources(Pattern.compile(".*\\.json"));

        // TODO: Check whether the same mapper can read both JSON & YAML
        if (!yamlResourceNames.isEmpty()) {
            log.debug("Collected {} legacy (yaml) module-infos: {}", yamlResourceNames.size(), yamlResourceNames);
            log.warn("The YAML format is deprecated, please use JSON according to documentation");
        }
        log.debug("Discovered {} module-infos to process: {}", jsonResourceNames.size(), jsonResourceNames);

        for (String yamlResourceName : yamlResourceNames) {
            if (nonModuleInfoFileNames.contains(yamlResourceName)) {
                continue;
            }

            InputStream inputStream = getClass().getResourceAsStream("/" + yamlResourceName);
            if (inputStream == null) {
                throw new OsfException("Unexpected: input stream is null.");
            }
            String yamlContent = new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));
            try {
                LegacyModuleInfo legacy = yamlMapper.readValue(yamlContent, LegacyModuleInfo.class);
                Map<String, String> mapDependencyToVersion = new HashMap<>();
                if (legacy.getDependencies() != null) {
                    legacy.getDependencies().forEach(dependency -> mapDependencyToVersion.put(dependency, "1.0"));
                }
                ModuleInfo moduleInfo = new ModuleInfo(legacy.getUniqueName(), Double.toString(legacy.getVersion()), List.of(legacy.getAssembly()), mapDependencyToVersion);
                boolean added = moduleInfos.add(moduleInfo);
                if (!added) {
                    log.error("[Terminating] duplication found for module: {} version: {}", moduleInfo.getUniqueName(), moduleInfo.getVersion());
                    System.exit(1);
                }
            } catch (JsonProcessingException e) {
                log.error("Failed reading resource as module-info: {}", yamlResourceName, e);
            }
        }

        for (String jsonResourceName : jsonResourceNames) {
            if (nonModuleInfoFileNames.contains(jsonResourceName)) {
                continue;
            }

            ObjectMapper jsonMapper = new ObjectMapper();
            try {
                InputStream inputStream = getClass().getResourceAsStream("/" + jsonResourceName);
                if (inputStream == null) {
                    throw new OsfException("Unexpected: input stream is null.");
                }
                String jsonContent = new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));
                ModuleInfo moduleInfo = jsonMapper.readValue(jsonContent, ModuleInfo.class);
                boolean added = moduleInfos.add(moduleInfo);
                if (!added) {
                    log.error("[Terminating] duplication found for module: {} version: {}", moduleInfo.getUniqueName(), moduleInfo.getVersion());
                    System.exit(1);
                }
            } catch (JsonProcessingException e) {
                log.error("[Not a module-info file?] Failed reading resource as module-info: {}", jsonResourceName, e);
            }
        }
        log.debug("Processed {} module-infos: {}", moduleInfos.size(), moduleInfos);
        return moduleInfos;
    }

}
