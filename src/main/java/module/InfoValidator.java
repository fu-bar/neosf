package module;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import structures.ModuleInfo;

import java.util.*;

public class InfoValidator {
    private final Logger log = LogManager.getLogger(InfoValidator.class);
    private final Set<ModuleInfo> moduleInfoSet;
    private final Map<String, ModuleInfo> mapUniqueIdToModuleInfo = new HashMap<>();
    private final Map<String, String> osfBundledModules;
    private final Map<String, String> userRequiredModules;

    public InfoValidator(Set<ModuleInfo> moduleInfos,
                         Map<String, String> osfBundledModules,
                         Map<String, String> userRequiredModules) {
        this.moduleInfoSet = moduleInfos;
        this.osfBundledModules = osfBundledModules;
        this.userRequiredModules = userRequiredModules;
    }

    public List<String> getViolations() {
        List<String> violations = new ArrayList<>();

        log.debug("Validating {} module-infos", moduleInfoSet.size());
        for (ModuleInfo moduleInfo : this.moduleInfoSet) {
            ModuleInfo previousValue = mapUniqueIdToModuleInfo.put(moduleInfo.getUniqueId(), moduleInfo);
            if (previousValue != null) {
                violations.add(String.format("The unique id of %s was met more than once", moduleInfo.getUniqueId()));
            }
        }

        for (Map.Entry<String, ModuleInfo> uniqueIdToInfo : this.mapUniqueIdToModuleInfo.entrySet()) {
            ModuleInfo moduleInfo = uniqueIdToInfo.getValue();

            moduleInfo.getDependsOn().forEach((key, value) -> {
                if (!this.mapUniqueIdToModuleInfo.containsKey(ModuleInfo.getUniqueId(key, value))) {
                    violations.add(String.format("Module %s requires module %s but no such module discovered", moduleInfo.getUniqueId(), ModuleInfo.getUniqueId(key, value)));
                }
            });

        }

        Map<String, String> requiredModules = new HashMap<>(osfBundledModules);
        requiredModules.putAll(userRequiredModules);

        for (Map.Entry<String, String> bundledModule : requiredModules.entrySet()) {
            String uniqueId = ModuleInfo.getUniqueId(bundledModule.getKey(), bundledModule.getValue());
            if (!this.mapUniqueIdToModuleInfo.containsKey(uniqueId)) {
                violations.add(String.format("Required module %s not found in classpath", uniqueId));
            }
        }

        if (!violations.isEmpty()) {
            log.error("{} violations found: {}", violations.size(), violations);
        }
        return violations;
    }
}
