package interfaces;

import structures.ClassInfo;
import structures.ModuleInfo;

import java.util.Collection;
import java.util.List;

public interface DIManagerInterface {
    <T> T getComponent(Class<T> componentType);

    void processModuleInfos(Collection<ModuleInfo> moduleInfos);

    List<ClassInfo> getOsfStartableClassInfos();
}
