package ioc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.picocontainer.PicoCompositionException;
import org.picocontainer.PicoContainer;
import org.picocontainer.adapters.AbstractAdapter;
import org.picocontainer.injectors.InjectInto;
import osf.shared.annotations.OsfInternal;
import osf.shared.annotations.Singleton;
import osf.shared.exceptions.OsfException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.*;

/***
 * This class represents an adapter to control internal access
 */
public class OSFInternalAdapter extends AbstractAdapter<Object> {

    private final boolean isSingleton;
    private final Class<?> adaptedClass;
    private final transient List<Constructor<?>> adaptedConstructors;
    private final Map<Type, Boolean> mapTypeToInfrastructureAccess = new HashMap<>();
    private transient Object singletonInstance;
    private final transient Logger log = LogManager.getLogger(OSFInternalAdapter.class);

    /**
     * Constructor
     *
     * @param adaptedClass the adapted class
     */
    public OSFInternalAdapter(Class<?> adaptedClass) {
        super(adaptedClass, adaptedClass);
        this.adaptedClass = adaptedClass;
        this.isSingleton = (adaptedClass.getAnnotation(Singleton.class) != null);
        this.adaptedConstructors = Arrays.asList(adaptedClass.getDeclaredConstructors());
    }

    /***
     * {@inheritDoc}
     */
    @Override
    public Object getComponentKey() {
        return this.getComponentImplementation();
    }

    /***
     * {@inheritDoc}
     */
    @Override
    public Class<?> getComponentImplementation() {
        return this.adaptedClass;
    }

    private Object getInstance(PicoContainer container) {
        if (this.isSingleton) {
            if (this.singletonInstance == null) {
                this.singletonInstance = this.constructNewInstance(container);
            }
            return this.singletonInstance;
        } // Else
        return this.constructNewInstance(container);
    }

    private Object newInstance(Constructor<?> constructor, List<Object> parameters) {
        try {
            if (parameters.isEmpty()) {
                return constructor.newInstance();
            } else {
                return constructor.newInstance(parameters.toArray());
            }
        } catch (InstantiationException
                | InvocationTargetException
                | IllegalAccessException exception) {
            throw new OsfException(
                    String.format("Internal error: %s", exception.getMessage()), exception);
        }
    }

    private Object constructNewInstance(PicoContainer container) {
        // Starting with most specific constructor
        this.adaptedConstructors.sort(Comparator.comparing(Constructor::getParameterCount));
        Collections.reverse(this.adaptedConstructors);

        for (Constructor<?> constructor : this.adaptedConstructors) {
            if (constructor.getParameterCount() == 0) { // Default constructor
                return (this.newInstance(constructor, List.of()));
            }

            Class<?>[] parameterTypes = constructor.getParameterTypes();
            List<Object> parameters = new ArrayList<>(parameterTypes.length);
            for (Class<?> clazz : parameterTypes) {
                Object constructorParameter = container.getComponent(clazz);
                // Parameter required by the constructor is missing from the container
                if (constructorParameter == null) {
                    break;
                }
                parameters.add(constructorParameter);
            }
            if (parameters.size() != parameterTypes.length) { // Failed collecting all required parameters
                parameters.clear();
                continue;
            }
            return (this.newInstance(constructor, parameters));
        }
        throw new PicoCompositionException(
                String.format("Failed finding a suitable constructor for %s", this.adaptedClass.getName()));
    }

    private void setAccessPermission(InjectInto target) {
        Class<?> intoClass = target.getIntoClass();
        Annotation infrastructure = intoClass.getAnnotation(OsfInternal.class);
        this.mapTypeToInfrastructureAccess.put(target, infrastructure != null);
    }

    /**
     * Get component instance based on the specific type
     *
     * @param container    the container to be used
     * @param injectedInto the injected class information
     * @return the component instance
     */
    @Override
    public Object getComponentInstance(PicoContainer container, Type injectedInto) {

        // Injected-into class not yet encountered
        if (!this.mapTypeToInfrastructureAccess.containsKey(injectedInto)) {
            this.setAccessPermission((InjectInto) injectedInto);
        }

        // The injected-into class is allowed to receive the injection
        if (this.mapTypeToInfrastructureAccess.get(injectedInto)) {
            return this.getInstance(container);
        }

        // Injection request denied.
        log.error("{} access permission per class: {}", this.adaptedClass, this.mapTypeToInfrastructureAccess);
        return null;
    }

    /***
     * {@inheritDoc}
     */
    @Override
    public void verify(PicoContainer container) throws PicoCompositionException {
        this.constructNewInstance(container);
    }

    /***
     * {@inheritDoc}
     */
    @Override
    public String getDescriptor() {
        return "Allows injection only into infrastructure modules";
    }
}
