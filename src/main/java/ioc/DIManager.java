package ioc;

import helpers.ReflectionHelper;
import interfaces.DIManagerInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import osf.shared.annotations.InjectService;
import osf.shared.annotations.OsfInternal;
import osf.shared.annotations.Singleton;
import osf.shared.interfaces.LogLevelSetterInterface;
import osf.shared.interfaces.LogProviderInterface;
import osf.shared.interfaces.OSFStartable;
import structures.ClassInfo;
import structures.ModuleInfo;

import java.util.*;

import static org.picocontainer.Characteristics.*;

public class DIManager implements DIManagerInterface {
    private final Logger log = LogManager.getLogger(DIManager.class);
    private final MutablePicoContainer picoContainer = new DefaultPicoContainer();
    private final Set<ClassInfo> registeredClasses = new HashSet<>();
    private final Set<ClassInfo> osfStartableClasses = new HashSet<>();
    private final ReflectionHelper reflectionHelper = new ReflectionHelper();

    public DIManager(String serviceId) {
        log.debug("serviceId value will be registered as {}", serviceId);
        picoContainer.as(USE_NAMES).addConfig("serviceId", serviceId);
        picoContainer.as(HIDE_IMPL).addComponent(LogProviderInterface.class, LoggingCore.class);
        picoContainer.as(HIDE_IMPL).addComponent(LogLevelSetterInterface.class, LoggingCore.class);
    }

    @Override
    public <T> T getComponent(Class<T> componentType) {
        return picoContainer.getComponent(componentType);
    }

    @Override
    public void processModuleInfos(Collection<ModuleInfo> moduleInfos) {
        for (ModuleInfo moduleInfo : moduleInfos) {
            List<String> indicatedPackages = moduleInfo.getPackages();
            log.debug("Reflecting: {} for {}", indicatedPackages, moduleInfo.getUniqueId());
            for (String pkg : indicatedPackages) {
                this.processPackage(pkg);
            }
        }
    }

    private void processPackage(String pkg) {
        List<String> registered = new ArrayList<>();

        for (Class<?> clazz : reflectionHelper.getAnnotatedClasses(pkg, InjectService.class)) {
            ClassInfo classInfo = inspect(clazz);
            if (classInfo.isInjectable()) {
                registerForInjection(classInfo);
                registered.add(classInfo.getClazz().getName());
            }
        }
        log.debug("Total of {} classes in package {} were registered for injection: {}", registered.size(), pkg, registered);
    }

    private ClassInfo inspect(Class<?> clazz) {
        boolean isInjectable = clazz.getAnnotation(InjectService.class) != null;
        boolean isSingleton = clazz.getAnnotation(Singleton.class) != null;
        boolean isOsfInternal = clazz.getAnnotation(OsfInternal.class) != null;
        boolean isOsfStartable = OSFStartable.class.isAssignableFrom(clazz);
        ClassInfo classInfo = new ClassInfo(clazz, isInjectable, isSingleton, isOsfInternal, isOsfStartable);
        log.debug("Class info: {}", classInfo);
        return classInfo;
    }

    private void registerForInjection(ClassInfo classInfo) {
        if (registeredClasses.contains(classInfo)) {
            return; // Already registered
        }

        if (classInfo.isOSFStartable()) {
            osfStartableClasses.add(classInfo);
        }

        if (classInfo.isOsfInternal()) { // OSFInternalAdapter takes care of singletons internally
            picoContainer.addAdapter(new OSFInternalAdapter(classInfo.getClazz()));
        } else if (classInfo.isSingleton()) {
            picoContainer.as(SINGLE).addComponent(classInfo.getClazz());
        } else {
            picoContainer.addComponent(classInfo.getClazz());
        }

        registeredClasses.add(classInfo);
        log.debug("{} registered for injection", classInfo.getClazz().getName());
    }

    @Override
    public List<ClassInfo> getOsfStartableClassInfos() {
        return new ArrayList<>(osfStartableClasses);
    }
}
