package ioc;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import osf.shared.annotations.InjectService;
import osf.shared.annotations.Singleton;
import osf.shared.interfaces.LogLevelSetterInterface;
import osf.shared.interfaces.LogProviderInterface;

/**
 * Provides logger instances and sets logger levels in runtime
 */
@Singleton
@InjectService
class LoggingCore implements LogProviderInterface, LogLevelSetterInterface {
    private static final Logger log = LogManager.getLogger(LoggingCore.class);

    @Override
    public void setLevel(Level level) {
        setLevel("", level);
    }

    @Override
    public void setLevel(String loggerName, Level level) {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration logConfig = ctx.getConfiguration();
        LoggerConfig loggerConfig = logConfig.getLoggerConfig(loggerName);
        log.debug(String.format("Setting log-level for %s to: %s%n", loggerName, level));
        loggerConfig.setLevel(level);
        ctx.updateLoggers();
    }

    @Override
    public void setLevel(Class<?> clazz, Level level) {
        setLevel(clazz.getName(), level);
    }

    @Override
    public Logger getLogger(String name) {
        return LogManager.getLogger(name);
    }

    @Override
    public Logger getLogger(Class<?> clazz) {
        return LogManager.getLogger(clazz);
    }
}
