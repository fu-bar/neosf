package helpers;


import com.google.common.reflect.ClassPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("UnstableApiUsage")
public class ReflectionHelper {

    private static final Logger log = LogManager.getLogger(ReflectionHelper.class);
    private static final String MATCH_ALL_REGEX = ".*";
    private URLClassLoader classLoader;

    private static URL[] getJarUrls(Collection<String> rootFolders, String regexMatch) {
        Pattern pattern = Pattern.compile(regexMatch);
        List<URL> urls = new ArrayList<>();
        for (String rootFolder : rootFolders) {
            log.debug(String.format("Discovering jar files recursively under %s", rootFolder));
            try (Stream<Path> pathStream = Files.find(
                    Paths.get(rootFolder),
                    10,
                    (path, basicFileAttributes) ->
                            basicFileAttributes.isRegularFile() &&
                                    path.toString()
                                            .toLowerCase()
                                            .endsWith("jar")
            )) {
                List<Path> jarPaths = pathStream.filter(path -> pattern.matcher(path.toString()).matches())
                        .collect(Collectors.toList());
                for (Path path : jarPaths) {
                    urls.add(path.toUri().toURL());
                }
            } catch (Exception e) {
                log.error(String.format("[Terminating] Unexpected error while collecting jar files : %s", e.getMessage()), e);
                System.exit(1);
            }
        }
        return urls.toArray(new URL[0]);
    }

    public URL[] discoverJars(Collection<String> rootPaths) {
        return discoverJars(rootPaths, MATCH_ALL_REGEX);
    }

    public URL[] discoverJars(Collection<String> rootPaths, String regexMatch) {
        URL[] jarUrls = getJarUrls(rootPaths, regexMatch);
        if (this.classLoader == null) {
            classLoader = URLClassLoader.newInstance(jarUrls);
        }
        if (!regexMatch.equals(MATCH_ALL_REGEX)) {
            log.info(String.format("Using regex filter: %s", regexMatch));
        }
        return jarUrls;
    }

    public Optional<Class<?>> loadClass(String classFQDN) {
        try {
            return Optional.of(classLoader.loadClass(classFQDN));
        } catch (ClassNotFoundException e) {
            return Optional.empty();
        }
    }

    public Set<Class<?>> getAnnotatedClasses(String packageName, Class<? extends Annotation> annotationClass) {

        Set<Class<?>> annotatedClasses = new HashSet<>();

        List<ClassLoader> classLoaders = new ArrayList<>();
        classLoaders.add(ClassLoader.getSystemClassLoader());
        if (classLoader != null) {
            classLoaders.add(classLoader);
        }
        classLoaders.forEach(loader -> {

            if (loader == null) {
                return; // Local class-loader may be not initialized
            }

            ClassPath classpath = null;

            try {
                classpath = ClassPath.from(loader);
            } catch (IOException e) {
                log.error("[Terminating] Failed creating class-path from loader", e);
                System.exit(1);
            }

            for (ClassPath.ClassInfo classInfo : classpath.getTopLevelClasses()) {
                if (!packageName.equals(classInfo.getPackageName())) {
                    continue;
                }
                try {
                    Class<?> theClass = classInfo.load();
                    Annotation declaredAnnotation = theClass.getDeclaredAnnotation(annotationClass);
                    if (declaredAnnotation != null) {
                        annotatedClasses.add(theClass);
                    }
                } catch (Exception exception) {
                    log.error(String.format("Failed loading: %s", classInfo.getName()), exception);
                }
            }
        });

        return annotatedClasses;
    }
}
