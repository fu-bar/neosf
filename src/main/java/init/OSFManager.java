package init;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.ServiceManager;
import config.OSFConfig;
import config.OSFManifest;
import interfaces.DIManagerInterface;
import ioc.DIManager;
import module.InfoCollector;
import module.InfoValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import osf.shared.exceptions.OsfException;
import osf.shared.interfaces.OSFStartable;
import structures.ClassInfo;
import structures.ModuleInfo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@SuppressWarnings("UnstableApiUsage")
public class OSFManager {
    private static final Logger log = LogManager.getLogger(OSFManager.class);
    private final ExecutorService serviceListenerExecutor = Executors.newSingleThreadExecutor();
    private DIManagerInterface diManager;
    private final OSFConfig osfConfig;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final OSFManifest osfManifest = loadOsfManifest();
    private ServiceManager serviceManager;

    public OSFManager(String serviceId) {
        Thread.setDefaultUncaughtExceptionHandler((throwable, exception) -> unhandledExceptionHandler(exception));
        this.osfConfig = new OSFConfig(serviceId, null);
    }

    public OSFManager(File osfHomeFolderPath) {
        Thread.setDefaultUncaughtExceptionHandler((throwable, exception) -> unhandledExceptionHandler(exception));
        File configFile = new File(osfHomeFolderPath, "config.json");
        log.debug("Reading service configuration from: {}", configFile.getAbsolutePath());
        if (!configFile.exists() || configFile.isDirectory()) {
            log.error("[Terminating] Not a configuration file: {}", configFile.getAbsolutePath());
            System.exit(1);
        }
        try {
            this.osfConfig = objectMapper.readValue(configFile, OSFConfig.class);
        } catch (IOException e) {
            throw new OsfException(String.format("Failed reading %s as an OSF configuration file", configFile.getAbsolutePath()), e);
        }
    }

    public OSFManager(OSFConfig osfConfig) {
        Thread.setDefaultUncaughtExceptionHandler((throwable, exception) -> unhandledExceptionHandler(exception));
        this.osfConfig = osfConfig;
    }

    public void init() {
        log.debug("Initializing OSF version {} with bundled modules {}", osfManifest.getVersion(), osfManifest.getBundledModules());
        InfoCollector infoCollector = new InfoCollector();
        Set<ModuleInfo> moduleInfos = infoCollector.collectInfoOnModules();
        InfoValidator infoValidator = new InfoValidator(moduleInfos, osfManifest.getBundledModules(), osfConfig.getUserRequiredModule());
        List<String> violations = infoValidator.getViolations();
        if (!violations.isEmpty()) {
            log.error("[Terminating] Found {} violations: {}", violations.size(), violations);
            System.exit(1);
        }

        this.diManager = new DIManager(osfConfig.getServiceId());
        this.diManager.processModuleInfos(moduleInfos);

        this.startAutoRunnableClasses();
    }

    public void shutdown() {
        log.info("Shutting down (timeout in {} seconds)", osfConfig.getSecondsToWaitForShutdown());
        serviceManager.stopAsync();
        try {
            serviceManager.awaitStopped(osfConfig.getSecondsToWaitForShutdown(), TimeUnit.SECONDS);
            log.info("Shutdown success.");
            System.exit(0);
        } catch (TimeoutException e) {
            log.warn("[Terminating] Timeout while waiting {} seconds for auto-runnable modules to stop.", osfConfig.getSecondsToWaitForShutdown());
            System.exit(1);
        }
    }

    public ImmutableSetMultimap<Service.State, Service> getRunnableModulesByStatus() {
        return this.serviceManager.servicesByState();
    }

    private void startAutoRunnableClasses() {
        List<ClassInfo> osfStartableClasses = this.diManager.getOsfStartableClassInfos();
        log.debug("{} classes are OSF-Startable: {}", osfStartableClasses.size(), osfStartableClasses.stream().map(classInfo -> classInfo.getClazz().getSimpleName()).collect(Collectors.toList()));
        Set<Service> services = new HashSet<>();
        for (ClassInfo classInfo : osfStartableClasses) {
            OSFStartable osfStartable = (OSFStartable) this.diManager.getComponent(classInfo.getClazz());
            services.add(osfStartable);
        }
        serviceManager = new ServiceManager(services);
        ServiceManager.Listener serviceListener = new ServiceManager.Listener() {
            @Override
            public void stopped() {
                super.stopped();
                log.debug("All runnable modules are stopped");
            }

            @Override
            public void failure(@NotNull Service service) {
                super.failure(service);
                serviceListenerExecutor.shutdownNow();
                log.error("{}", service, service.failureCause());
            }
        };

        serviceManager.addListener(serviceListener, serviceListenerExecutor);
        log.debug("Starting all osf-runnable modules asynchronously (timeout in {} seconds): {}", osfConfig.getSecondsToWaitForInit(), services);
        serviceManager.startAsync();
        try {
            serviceManager.awaitHealthy(osfConfig.getSecondsToWaitForInit(), TimeUnit.SECONDS);
            log.info("All runnable services are running: {}", serviceManager.servicesByState());
        } catch (TimeoutException e) {
            log.error("Timeout awaiting all runnable modules to start for {} seconds: {}", osfConfig.getSecondsToWaitForInit(), serviceManager.servicesByState());
        }
    }

    private OSFManifest loadOsfManifest() {
        URL osfJsonURL = Resources.getResource("osf.json");
        try {
            String osfManifestContent = Resources.toString(osfJsonURL, StandardCharsets.UTF_8);
            return objectMapper.readValue(osfManifestContent, OSFManifest.class);
        } catch (IOException e) {
            throw new OsfException("[Terminating] failed loading OSF manifest");
        }
    }

    private void unhandledExceptionHandler(Throwable exception) {
        log.error("[Terminating] Unhandled exception {}: {}", exception.getClass().getSimpleName(), exception.getMessage(), exception);
        System.exit(1);
    }
}
