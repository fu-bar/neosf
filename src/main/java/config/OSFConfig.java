package config;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class OSFConfig {
    private final Logger log = LogManager.getLogger(OSFConfig.class);
    private String serviceId;
    private String log4j2ConfigFilePath;
    private int secondsToWaitForShutdown = 15;
    private int secondsToWaitForInit = 30;
    private Map<String, String> userRequiredModule = new HashMap<>();
    private static final String LOGO = "\n" +
            "╭━━━╮  ╭━━━╮  ╭━━━╮\n" +
            "┃╭━╮┃  ┃╭━╮┃  ┃╭━━╯\n" +
            "┃┃ ┃┃  ┃╰━━╮  ┃╰━━╮\n" +
            "┃┃ ┃┃  ╰━━╮┃  ┃╭━━╯\n" +
            "┃╰━╯┃╭╮┃╰━╯┃╭╮┃┃\n" +
            "╰━━━╯╰╯╰━━━╯╰╯╰╯";

    public OSFConfig(String serviceId, String log4j2ConfigFilePath) {
        System.out.println(LOGO);
        this.setServiceId(serviceId);
        this.setLog4j2ConfigFilePath(log4j2ConfigFilePath);
    }

    private OSFConfig() {
        System.out.println(LOGO);
        // For deserialization
    }

    public String getLog4j2ConfigFilePath() {
        return log4j2ConfigFilePath;
    }

    public void setLog4j2ConfigFilePath(String log4j2ConfigFilePath) {
        if (log4j2ConfigFilePath == null) {
            log.warn("Log4j2 configuration file path is not set (falling back to defaults)");
            return;
        }
        this.log4j2ConfigFilePath = log4j2ConfigFilePath;
        configLogger(log4j2ConfigFilePath);
    }

    public String getServiceId() {
        return serviceId.strip(); // White space is assumed to be unintentional
    }

    public void setServiceId(String serviceId) {
        if (serviceId == null || serviceId.strip().isEmpty()) {
            log.error("[Terminating] invalid service-id value (may not be null nor white-space");
            System.exit(1);
        }
        log.debug("Setting service id to: {}", serviceId);
        this.serviceId = serviceId;
    }

    public Map<String, String> getUserRequiredModule() {
        return userRequiredModule;
    }

    public void setUserRequiredModule(Map<String, String> userRequiredModule) {
        this.userRequiredModule = userRequiredModule;
    }

    public int getSecondsToWaitForShutdown() {
        return secondsToWaitForShutdown;
    }

    public void setSecondsToWaitForShutdown(int secondsToWaitForShutdown) {
        this.secondsToWaitForShutdown = secondsToWaitForShutdown;
    }

    public int getSecondsToWaitForInit() {
        return secondsToWaitForInit;
    }

    public void setSecondsToWaitForInit(int secondsToWaitForInit) {
        this.secondsToWaitForInit = secondsToWaitForInit;
    }

    private void configLogger(String log4jConfigFilePath) {
        File logConfigFile = new File(log4jConfigFilePath);
        if (!logConfigFile.exists() || logConfigFile.isDirectory()) {
            log.error("[Terminating] Not a log4j2 configuration file: {}", logConfigFile.getAbsolutePath());
            System.exit(1);
        }
        log.debug(String.format("Configuring log4j2 using file: %s", log4jConfigFilePath));
        Configurator.initialize(null, log4jConfigFilePath);
    }
}
