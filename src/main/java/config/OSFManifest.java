package config;

import java.util.HashMap;
import java.util.Map;

public class OSFManifest {
    private String version;
    private Map<String, String> bundledModules = new HashMap<>();

    public OSFManifest(String version, Map<String, String> bundledModules) {
        this.version = version;
        this.bundledModules = bundledModules;
    }

    private OSFManifest() {
        // For deserialization
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Map<String, String> getBundledModules() {
        return bundledModules;
    }

    public void setBundledModules(Map<String, String> bundledModules) {
        this.bundledModules = bundledModules;
    }
}
