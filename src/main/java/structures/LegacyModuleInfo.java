package structures;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LegacyModuleInfo {
    @JsonProperty("unique-name")
    private String uniqueName;

    @JsonProperty("qualified-name")
    private String qualifiedName;

    private double version;

    private String assembly;

    private List<String> dependencies;

    public String getUniqueName() {
        return uniqueName;
    }

    public String getQualifiedName() {
        return qualifiedName;
    }

    public double getVersion() {
        return version;
    }

    public String getAssembly() {
        return assembly;
    }

    public List<String> getDependencies() {
        return dependencies;
    }
}
