package structures;

import com.google.common.base.Objects;

public class ClassInfo {
    private final Class<?> clazz;
    private final boolean isInjectable;
    private final boolean isSingleton;
    private final boolean isOsfInternal;
    private final boolean isOSFStartable;

    @Override
    public String toString() {
        return "ClassInfo{" +
                "clazz=" + clazz +
                ", isInjectable=" + isInjectable +
                ", isSingleton=" + isSingleton +
                ", isOsfInternal=" + isOsfInternal +
                ", isOSFStartable=" + isOSFStartable +
                '}';
    }

    public ClassInfo(Class<?> clazz, boolean isInjectable, boolean isSingleton, boolean isOsfInternal, boolean isOSFStartable) {
        this.clazz = clazz;
        this.isInjectable = isInjectable;
        this.isSingleton = isSingleton;
        this.isOsfInternal = isOsfInternal;
        this.isOSFStartable = isOSFStartable;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public boolean isInjectable() {
        return isInjectable;
    }

    public boolean isSingleton() {
        return isSingleton;
    }

    public boolean isOsfInternal() {
        return isOsfInternal;
    }

    public boolean isOSFStartable() {
        return isOSFStartable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassInfo classInfo = (ClassInfo) o;
        return Objects.equal(clazz, classInfo.clazz);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(clazz);
    }
}
