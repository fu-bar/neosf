package structures;

import com.google.common.base.Objects;

import java.util.List;
import java.util.Map;

public class ModuleInfo {
    private final String uniqueName;
    private final String version;
    private final List<String> packages;
    private final Map<String, String> dependsOn;

    public ModuleInfo() {
        uniqueName = null;
        version = null;
        packages = null;
        dependsOn = null;
    }

    public ModuleInfo(String uniqueName, String version, List<String> packages, Map<String, String> dependsOn) {
        this.uniqueName = uniqueName;
        this.version = version;
        this.packages = packages;
        this.dependsOn = dependsOn;
    }

    public static String getUniqueId(String uniqueName, String version) {
        return String.format("%s_%s", uniqueName, version);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModuleInfo that = (ModuleInfo) o;
        return Objects.equal(uniqueName, that.uniqueName) && Objects.equal(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uniqueName, version);
    }

    public String getUniqueId() {
        return ModuleInfo.getUniqueId(getUniqueName(), getVersion());
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public String getVersion() {
        return version;
    }

    public List<String> getPackages() {
        return packages;
    }

    public Map<String, String> getDependsOn() {
        return dependsOn;
    }

    @Override
    public String toString() {
        return "ModuleInfo{" +
                "uniqueName='" + uniqueName + '\'' +
                ", version='" + version + '\'' +
                ", packages=" + packages +
                ", dependsOn=" + dependsOn +
                '}';
    }
}
